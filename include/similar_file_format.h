#ifndef SIMILAR_FILE_FORMAT_H_
#define SIMILAR_FILE_FORMAT_H_

#include "datastructures.h"
#include "similar_std.h"
#include <stddef.h>

// Helpers
// --------------
struct SimilarCoordinate {
  int x;
  int y;
};

// Determines the direction of the wire
enum SimilarWireEndInfo { WIRE_INPUT, WIRE_OUTPUT };

// Similar Primitives (Components, wires, etc...)
// -------------------------------------------------
struct SimilarWire {
  struct SimilarCoordinate startPosition;
  struct SimilarCoordinate endPosition;
};

struct SimilarComponent {
  enum SimilarComponentId id;
  struct SimilarCoordinate position;
};

struct SimilarConnection {
  TLList *wires;
};

struct SimilarFile {
  char *name;
  // Components
  TLList *components;
  // Connections
  TLList *connections;
};

#endif // SIMILAR_FILE_FORMAT_H_
