#ifndef DATASTRUCTURES_H_
#define DATASTRUCTURES_H_

#include <stddef.h>

typedef struct LinkedListNode {
  struct LinkedListNode *next;
  void *content;
} TLNode;
typedef TLNode TLList;

TLList *LL_Create(void *content);
void LL_Destroy(TLList *l);
void LL_Add(TLList **l, void *content);
size_t LL_Length(TLList const *l);
void **LL_ToArray(TLList const *l);
char *
LL_ToCharArray(TLList const *l); // Needed since char is smaller and otherwise
                                 // the c std string lib doesn't work properly

#endif // DATASTRUCTURES_H_
