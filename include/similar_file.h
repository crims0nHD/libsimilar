#ifndef SIMILAR_FILE_H_
#define SIMILAR_FILE_H_

#include "similar_file_format.h"
#include "similar_std.h"

int libSimilar_ReadFile(char const *path, struct SimilarFile **sf);
void libSimilar_PrintComponents(struct SimilarFile const *const sf);

#endif // SIMILAR_FILE_H_
