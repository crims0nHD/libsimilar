#include "datastructures.h"

#include <stddef.h>
#include <stdlib.h>

// Create a new linked list
void LL_Add(TLList **l, void *content) {
  TLNode *l_ = *l;
  TLNode *lnew = malloc(sizeof(TLNode));
  lnew->content = content;
  lnew->next = NULL;

  if (l_ != NULL) {
    while (l_->next != NULL)
      l_ = l_->next;
    l_->next = lnew;
  } else {
    *l = lnew;
  }
}

size_t LL_Length(const TLList *const list) {
  TLList const *l = list;
  if (l == NULL)
    return 0;
  size_t len = 0;
  do {
    len++;
  } while ((l = l->next));

  return len;
}

void **LL_ToArray(TLList const *l) {
  size_t len = LL_Length(l);
  if (len == 0)
    return NULL;
  void **ret = malloc(len * sizeof(void *));
  TLList *curr = l; // create a pointer to iterate over the list
  for (size_t i = 0; i < len; i++) {
    ret[i] =
        curr->content; // assign the element at index i of the list to the array
    curr = curr->next; // move to the next element in the list
  }
  return ret;
}

char *LL_ToCharArray(TLList const *l) {
  size_t len = LL_Length(l);
  if (len == 0)
    return NULL;
  char *ret = malloc(len * sizeof(char));
  TLList *curr = l; // create a pointer to iterate over the list
  for (size_t i = 0; i < len; i++) {
    ret[i] =
        curr->content; // assign the element at index i of the list to the array
    curr = curr->next; // move to the next element in the list
  }
  return ret;
}

void LL_Destroy(TLList *l) {
  if (l == NULL)
    return;
  TLNode *tmpNode = l;
  while (l != NULL) {
    tmpNode = l->next;
    free(l);
    l = tmpNode;
  }
}
