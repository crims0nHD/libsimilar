#include "datastructures.h"
#include "similar_file_format.h"
#include "similar_std.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

static int processComponent(char **similarFileLine, TLNode **components_tail) {
  struct SimilarComponent *comp = malloc(sizeof(struct SimilarComponent));

  // Map properties
  comp->id = atoi(similarFileLine[1]);
  comp->position.x = atoi(similarFileLine[3]);
  comp->position.y = atoi(similarFileLine[4]);

  // Debug
  printf("Found component: at x:%d y:%d\n", comp->position.x, comp->position.y);

  LL_Add(components_tail, comp);
  return 0;
}

static int processIO(char **similarFileLine, TLNode **components_tail) {
  struct SimilarComponent *comp = malloc(sizeof(struct SimilarComponent));

  // Map properties
  comp->id = atoi(similarFileLine[1]);
  comp->position.x = atoi(similarFileLine[3]);
  comp->position.y = atoi(similarFileLine[4]);

  // Debug
  printf("Found io: at x:%d y:%d\n", comp->position.x, comp->position.y);

  LL_Add(components_tail, comp);
  return 0;
}

static int processWire(char **similarFileLine, TLNode **connections_tail) {}

// csvMatrix[n][m]: Pointer to a Matrix that is NULL terminated on either
// dimension
//                  n: rows
//                  m: colums
static int Similar2CSV(FILE *f, char ****csvMatrix) {
  TLList *lines = NULL;
  char c = 'A';
  bool endOfFileReached = false;
  bool endOfLineReached = false;
  bool endOfWordReached = false;

  do {
    // New Line starts
    TLList *columns = NULL;
    endOfLineReached = false;
    do {
      // New Word starts
      TLList *word = NULL;
      endOfWordReached = false;
      while (!endOfWordReached) {
        c = fgetc(f);
        switch (c) {
        case -1:
          endOfFileReached = true;
        case '\n':
          endOfLineReached = true;
        case ' ':
          endOfWordReached = true;
          break;
        default:
          LL_Add(&word, (void *)c);
          break;
        }
      }
      c = 'A';
      LL_Add(&word, NULL);
      char *wordarray = LL_ToCharArray(word);
      LL_Destroy(word);
      LL_Add(&columns, wordarray);
    } while (!endOfLineReached);
    LL_Add(&columns, NULL);
    char **columnsarray = LL_ToArray(columns);
    LL_Destroy(columns);
    LL_Add(&lines, columnsarray);
  } while (!endOfFileReached);
  LL_Add(&lines, NULL);
  *csvMatrix = LL_ToArray(lines);
  LL_Destroy(lines);
  return 0;
}

int libSimilar_ReadFile(char const *path, struct SimilarFile **sf) {
  FILE *similarFileStream = fopen(path, "r");
  if (similarFileStream == NULL)
    return -1;

  // Set up the datastructure
  // sf_ is the temporary casting of the pointer
  *sf = malloc(sizeof(struct SimilarFile));
  struct SimilarFile *sf_ = *sf;
  sf_->name = "TODO Lol";
  sf_->connections = NULL;
  sf_->components = NULL;

  char ***csv = NULL;
  Similar2CSV(similarFileStream, &csv);

  // Now comes the fun part:
  // Mapping stuff
  for (size_t i = 4; csv[i] != NULL; i++) {
    size_t tmp = atoi(csv[i][0]);
    printf("Processing instruction %li:\n", tmp);
    switch (tmp) {
    case SIMILAR_INSTRUCTION_COMPONENT:
      processComponent(csv[i], &(sf_->components));
      break;
    }
  }

  fclose(similarFileStream);

  return 0;
}

void libSimilar_PrintComponents(struct SimilarFile const *const sf) {
  if (sf == NULL)
    return;
  TLNode *tmpNode = sf->components;
  while (tmpNode != NULL) {
    printf("Type of component ");
    struct SimilarComponent *tmpComponent = tmpNode->content;
    switch (tmpComponent->id) {
    case SIMILAR_COMPONENT_AND:
      printf("AND gate at position: x:%i y:%i\n", tmpComponent->position.x,
             tmpComponent->position.y);
      break;
    default:
      break;
    }
    tmpNode = tmpNode->next;
  }
}
